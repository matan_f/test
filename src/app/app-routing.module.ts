import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ByeComponent } from './bye/bye.component';
import { CustomersComponent } from './customers/customers.component';



const routes: Routes = [
 
  { path: 'login', component: LoginComponent},
  { path: 'books', component: BooksComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'bye', component: ByeComponent },
  { path: 'customers', component: CustomersComponent }


];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
