import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers$; 
  customers:Customer[];
  userId:string; 
  editstate = [];
  addCustomerFormOPen = false;
  panelOpenState = false;

  constructor(private customersService:CustomersService, public authService:AuthService) { }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id); 
  }

  
  update(customer:Customer){
    this.customersService.updateCustomer(this.userId,customer.id ,customer.name, customer.years,customer.income);
  }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.years,customer.income); 
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getCustomers(this.userId); 
        
        this.customers$.subscribe(
          docs =>{
            console.log('init worked');
            // this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            // this.firstDocumentArrived = docs[0].payload.doc;
            // this.push_prev_startAt(this.firstDocumentArrived);             
            this.customers = [];
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 
      }
    )

  }

}
