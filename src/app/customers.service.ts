import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  // public addBooks(){
  //   setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'Short summary'}),2000);
  // }

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`, 
    ref => ref.orderBy('name', 'asc')); 
    return this.customerCollection.snapshotChanges()      
  } 


  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 

  addCustomer(userId:string,name:string,years:number,income:number){
    const customer = {name:name, years:years, income:income}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  updateCustomer(userId:string,id:string,name:string,years:number,income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income
      }
    )
  }

  constructor(private db:AngularFirestore) { }
}
