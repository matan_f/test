import { Customer } from './../interfaces/customer';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;  
  @Input() years:number; 
  @Input() income:number; 
  @Input() id:string; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
   
  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, years:this.years, income:this.income};
    this.update.emit(customer); 
    if(this.formType == "Add customer"){
      this.name  = null;
      this.years = null; 
      this.income = null; 
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}
